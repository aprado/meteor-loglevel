## Overview

A minimal lightweight console logger that preserves line numbers and supports per-package and app specific prefixes and log levels that are read from Meteor.settings. Based on the [loglevel.js](https://github.com/pimterry/loglevel) logging library.

## API summary

```javascript

// Creating logger
log = Loglevel({prefix: 'AppName:', level: 'debug'});

// Logging
log.trace(msg);
log.fine(obj);
log.debug(obj);
log.info(msg1 + msg2);
log.warn('obj=\n', obj);
log.error(err);

// Set the log level
log.setLevel(level); // Any of the levels above.
log.enableAll();
log.disableAll();
```

Logging methods will use the same console method or fallback to console.log, if not defined.


## Setting log levels in Meteor.settings

Just create a key under Meteor.settings.loglevel (server side) or Meteor.settings.public.loglevel (client side) with your app name:

```javascript

Meteor.settings.public.loglevel['myyapp']
```

Note that server side, if a key will not be found under Meteor.settings.loglevel, the client side log level will be used. This allows you to control the log level both server side and client side by setting it only under Meteor.settings.public.loglevel.

## Forcing a global log level

Meteor.settings.loglevel.global - server side. 

Meteor.settings.public.loglevel.global - client side (or if server side global isn't set, server side too).

This will overwrite all log levels for all loggers.

## Specifying a default log level

Meteor.settings.loglevel.default - server side. 

Meteor.settings.public.loglevel.default - client side (or if server side default isn't set, server side too).

This will be used, if no other log level was found in Meteor.settings.


## Source

loglevel.js - [MIT](https://github.com/pimterry/loglevel/blob/master/LICENSE-MIT)
