Package.describe({
  name: 'adrprado:loglevel',
  version: '0.0.2',
  // Brief, one-line summary of the package.
  summary: 'Simple logger with app and per-package log levels and line number preserving output.',
  // URL to the Git repository containing the source code for this package.
  git: 'git@bitbucket.org:aprado/meteor-loglevel.git',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.2.0.2');
  api.use('ecmascript');
  api.addFiles('loglevel.js');
  api.export(['Loglevel', 'logLevel']);
});

// Package.onTest(function(api) {
//   api.use('ecmascript');
//   api.use('tinytest');
//   api.use('adrprado:loglevel');
//   api.addFiles('loglevel-tests.js');
// });
